package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		String word = "dad";
		assertTrue("Unable to validate palindrome word", Palindrome.isPalindrome(word));
}	
	
	@Test
	public void testIsPalindromeNegative( ) {
		String word = "cat";
		assertFalse("Unable to validate palindrome word", Palindrome.isPalindrome(word));
}	
	
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		String word = "edit tide";
		assertTrue("Unable to validate palindrome word", Palindrome.isPalindrome(word));
}	
	
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		String word = "edit is tide";
		assertFalse("Unable to validate palindrome word", Palindrome.isPalindrome(word));
}	
	
	
	
	
	
//	@Test
//	public void testIsPalindrome( ) {
//		fail( "Test not created." );
//	}
//
//	@Test
//	public void testIsPalindromeNegative( ) {
//		fail( "Test not created." );
//	}
//	@Test
//	public void testIsPalindromeBoundaryIn( ) {
//		fail( "Test not created." );
//	}
//	@Test
//	public void testIsPalindromeBoundaryOut( ) {
//		fail( "Test not created." );
//	}	
	
}
